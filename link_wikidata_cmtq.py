#! /usr/bin/env python
"""link_wikidata_cmtq  Copyright (C) 2021  Cinemathèque québécoise
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under certain conditions."

This script adds statements in Wikidata which links people from
CQ's (Cinémathèque québécoise's) linked open data with people from Wikidata.

Usage:
  link_wikidata_cmtq.py [--label=<entityLabel>|--limit=<num>]
  link_wikidata_cmtq.py (-h | --help)
  link_wikidata_cmtq.py --version

Options:
  -h --help              Show this screen.
  --version              Show version.
  --label <entityLabel>  Label of CQ's entity to link to Wikidata.
  --limit <num>          Number of entities of CQ's database to link to Wikidata.
"""

from docopt import docopt
import pywikibot
from pywikibot import Site
from pywikibot import WbTime
from pywikibot.page import Claim
from pywikibot.page import ItemPage
from SPARQLWrapper import SPARQLWrapper, JSON
import argparse
from datetime import date
from typing import List, Union, Optional

CQ_SPARQL_ENDPOINT = "http://data.cinematheque.qc.ca/sparql"

CQ_LOD_ENTITY = "Q104486501"

PERSON_CQ_LINK_PROP = "P8971"

STATED_IN_PROP = "P248"

RETRIEVED_PROP = "P813"

class CqWdLink:
    """
    A link between a CQ resource and a Wikidata resource.

    A Wikidata resource is represented by it's QID (ex. Q2977617)

    A CQ resource is represented by it's ID (ex. 1063)
    A Wikibase item may be defined by either a 'Q' id (qid),
    or by a site & title.
    """

    def __init__(self, cq_entity_id, wd_entity_id, entity_label):
        self.entity_label = entity_label
        self.cq_entity_id = cq_entity_id
        self.wd_entity_id = wd_entity_id

def get_wd_cq_links(entityLabel : Optional[str], limit : Optional[int]) -> List[CqWdLink]:
    """
    Fetch links between CQ and Wikidata through CQ's SPARQL endpoint.

    Only links which are not already present in Wikidata will be
    fetched.

    @param n: First n links from CQ's unlinked data
    @return: List of CQ and Wikidata links
    """

    sparql = SPARQLWrapper(CQ_SPARQL_ENDPOINT)
    labelQueryPart = "" if entityLabel is None else f"?cqUri rdfs:label \"{entityLabel}\" ."
    limitQueryPart = ("" if limit is None else f"LIMIT {str(limit)}") if entityLabel is None else "LIMIT 1"
    # limitQueryPart = "" if limit is None else "LIMIT " + str(limit)
    query = f"""
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?cqUri ?cqEntityId ?wdEntityId ?entityLabel WHERE {{
  ?cqUri a crm:E21_Person .
  ?cqUri rdfs:label ?entityLabel .
  {labelQueryPart}
  ?cqUri crm:P48_has_preferred_identifier [ crm:P190_has_symbolic_content ?cqEntityId ] .
  BIND(xsd:string(?cqEntityId) as ?cqEntityIdStr)

  ?cqUri owl:sameAs ?wdUri .
  BIND(REPLACE(str(?wdUri), "http://www.wikidata.org/entity/", "", "i") AS ?wdEntityId)
  FILTER (contains(str(?wdUri), "http://www.wikidata.org/entity/"))

  MINUS {{
    SERVICE <https://query.wikidata.org/sparql> {{
      ?wdUri wdt:P8971 ?cqEntityIdStr
    }}
  }}
}} {limitQueryPart}
    """
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    return [CqWdLink(r["cqEntityId"]["value"], r["wdEntityId"]["value"], r["entityLabel"]["value"])
            for r in results["results"]["bindings"]]

# Add link statements to Wikidata
def add_links_wd(site : Site, cq_wd_links : List[CqWdLink]) -> None:
    """
    Fetches Wikidata item and adds statement linking it with CQ.

    If it fails to add the statement, it proceeds to link the next one.

    @param site: Wikidata site
    @param cq_wd_links: All known linked people between CQ and Wikidata
    """

    today = date.today()

    for cq_wd_link in cq_wd_links:
        try:
            add_link_wd(site, cq_wd_link, today)
        except Exception as e:
            print(f'ERROR: Could not add statement (http://data.cinematheque.qc.ca/resource/Person{cq_wd_link.cq_entity_id}, http://www.wikidata.org/prop/direct/{PERSON_CQ_LINK_PROP}, http://www.wikidata.org/entity/{cq_wd_link.wd_entity_id}).', e)
            continue

def add_link_wd(site : Site, cq_wd_link : CqWdLink, retrieved_date : date) -> None:
    """
    Adds statement linking a Wikidata item with CQ.

    @param site: Wikidata site
    @param item: Wikidata item to link
    @param cq_entity_id: CQ's resource ID
    """

    item = get_item_page(site, cq_wd_link.wd_entity_id)

    if item is None:
        print(f"Could not find Wikidata qid: {cq_wd_link.wd_entity_id}")
    elif is_already_linked(item, cq_wd_link.cq_entity_id):
        print(f"Wikidata qid {cq_wd_link.wd_entity_id} is already linked to {cq_wd_link.cq_entity_id}")
    else:
        cq_link_claim = mk_person_link_claim(site, cq_wd_link.cq_entity_id)
        statedin_claim = mk_statedin_cq_claim(site)
        retrieved_claim = mk_retrieved_claim(site, retrieved_date)
        cq_link_claim.addSource(statedin_claim)
        cq_link_claim.addSource(retrieved_claim)

        # Uncomment to actually add the statements in Wikidata
        item.addClaim(cq_link_claim, bot=True)

        print_info(cq_wd_link.entity_label, cq_wd_link.wd_entity_id, cq_wd_link.cq_entity_id, retrieved_date)

def get_item_page(site : Site, wd_entity_id : str) -> ItemPage:
    """
    Fetches Wikidata item from given qid.

    @param site: Wikidata site
    @param wd_entity_id: Wikidata item qid
    @return: Wikidata item page
    """

    if wd_entity_id is None:
        return None
    else:
        item = ItemPage(site.data_repository(), wd_entity_id)
        item.get()
        return item

def mk_person_link_claim(site : Site, cq_entity_id : str) -> Claim:
    """
    Create claim for linking a Wikidata person with CQ.

    @param site: Wikidata site
    @param cq_entity_id: CQ resource id
    @return: A claim
    """

    claim = Claim(site, PERSON_CQ_LINK_PROP)
    claim.setTarget(cq_entity_id)
    return claim

def mk_statedin_cq_claim(site : Site) -> Claim:
    """
    Create claim stating that another claim was retrieved from Cinematheque
    québécoise's LOD.

    @param site: Wikidata site
    @return: A claim
    """

    statedin_claim = Claim(site, STATED_IN_PROP, is_reference=True)
    statedin_claim.setTarget(ItemPage(site.data_repository(), CQ_LOD_ENTITY))
    return statedin_claim

def mk_retrieved_claim(site : Site, retrieved_date_data : date) -> Claim:
    """
    Create claim stating when another claim was retrieved from a database.

    @param site: Wikidata site
    @param retrieved_date_data: Date retrieved from a database
    @return: A claim
    """

    retrieved_claim = Claim(site, RETRIEVED_PROP, is_reference=True)
    wbTime = WbTime(
            retrieved_date_data.year,
            retrieved_date_data.month,
            retrieved_date_data.day)
    retrieved_claim.setTarget(wbTime)
    return retrieved_claim

def is_already_linked(item : ItemPage, cq_entity_id : str) -> bool:
    """
    Checks whether the Wikidata item is already linked to the correct CQ
    resource.

    @param item: Wikidata item
    @param cq_entity_id: CQ resource id
    @return: True the item is already linked to CQ. Else, False.
    """

    if PERSON_CQ_LINK_PROP in item.claims:
        person_cq_link_claims = item.claims[PERSON_CQ_LINK_PROP]
        return any([c.getTarget() == cq_entity_id
                    for c in person_cq_link_claims])
    return False

def print_info(entity_label : str, wd_entity_id : str, cq_entity_id : str, retrieved_date : date) -> None:
    """
    Prints to terminal statement that were added to Wikidata.

    @param wd_entity_id: Wikidata qid
    @param cq_entity_id: CQ resource id
    @param retrieved_date: Date that data was retrieved from CQ's LOD
    """

    print(f"Entity: {entity_label}")
    print(f"Added statement (http://www.wikidata.org/entity/{wd_entity_id}, http://www.wikidata.org/prop/direct/{PERSON_CQ_LINK_PROP}, http://data.cinematheque.qc.ca/resource/Person{cq_entity_id})")
    print(f"Added statement reference (http://www.wikidata.org/prop/direct/{STATED_IN_PROP}, http://www.wikidata.org/entity/{CQ_LOD_ENTITY})")
    print(f"Added statement qualifier (http://www.wikidata.org/prop/direct/{RETRIEVED_PROP}, {retrieved_date})")
    print()

def main(args) -> None:
    print(f"Fetching new links between Cinémathèque québécoise and Wikidata from '{CQ_SPARQL_ENDPOINT}'...")
    print()

    site = Site("wikidata", "wikidata")
    repo = site.data_repository()

    limit = None if args['--limit'] is None else int(args['--limit'])
    cq_wd_links = get_wd_cq_links(args['--label'], limit)

    if len(cq_wd_links) == 0:
        print(f"No new links between Cinémathèque québécoise and Wikidata will be inserted.\n")
    else:
        print(f"{len(cq_wd_links)} new link(s) between Cinémathèque québécoise and Wikidata will be inserted!\n")

    add_links_wd(site, cq_wd_links)

if __name__ == '__main__':
    # parser = argparse.ArgumentParser(description='Link entities between Cinémathèque québécoise and Wikidata.')
    # parser.add_argument('n', metavar='N', type=int, nargs='?',
    #                     help='number of entities to add to Wikidata')

    # args = parser.parse_args()
    # main(args)
    args = docopt(__doc__, version='link_wikidata_cmtq 1.0')
    main(args)
