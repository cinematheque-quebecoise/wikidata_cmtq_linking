# wikidata-cmtq-linking

Bot for automaticatly adding `P8971` (Cinémathèque québécoise person ID) statements which link people from Cinémathèque québécoise and Wikidata.

## Setup

This project was only tested in the GNU+Linux operating system.

### Dependencies

If you have Nix installed, simply initialize the dev environment with `nix-shell`.

If not, install `Python3` and `git`, and execute the following commands in the project's root:

1. `python -m venv .venv`
2. `source .venv/bin/activate`
3. `pip install -r requirements.txt`
4. `git submodule init`
5. `git submodule update`

### user-config.py generation

Details for generating the `user-config.py` file are found [here](https://www.mediawiki.org/wiki/Manual:Pywikibot/user-config.py).

First off, make sure you generated a bot name and password [here](https://www.mediawiki.org/wiki/Special:BotPasswords).

Then, execute `make user-config.py` and answer the questions.

Here's a sample:

```
[nix-shell:~/git/cq/wikidata_cmtq_linking]$ make user-config.py
NOTE: 'user-config.py' was not found!
Please follow the prompts to create it:
You can abort at any time by pressing ctrl-c

Your default user directory is "/home/username/git/cq/wikidata_cmtq_linking"
 1: commons
 2: foundation
 3: i18n
 4: incubator
 5: mediawiki
 6: meta
 7: omegawiki
 8: osm
 9: outreach
10: species
11: vikidia
12: wikibooks
13: wikidata
14: wikihow
15: wikimania
16: wikimediachapter
17: wikinews
18: wikipedia
19: wikiquote
20: wikisource
21: wikispore
22: wikitech
23: wikiversity
24: wikivoyage
25: wiktionary
26: wowwiki
Select family of sites we are working on, just enter the number or name (default: wikipedia): 13
This is the list of known languages:
beta, test, wikidata
The language code of the site we're working on: wikidata
Username on wikidata:wikidata: YourWikiUserName
Do you want to add any other projects? ([y]es, [N]o): N
Do you want to add a BotPassword for YourWikiUserName? ([y]es, [N]o, [q]uit): y
See https://www.mediawiki.org/wiki/Manual:Pywikibot/BotPasswords to
know how to get codes.Please note that plain text in
/home/username/git/cq/wikidata_cmtq_linking/user-password.py and anyone
with read access to that directory will be able read the file.
BotPassword's "bot name" for YourWikiUserName: YourBotName
BotPassword's "password" for "YourBotName" (no characters will be shown):
'/home/username/git/cq/wikidata_cmtq_linking/user-config.py' written.
'/home/username/git/cq/wikidata_cmtq_linking/user-password.py' written.
Now, you have to re-execute the command to start your script.
```

## Testing

Before thinking of executing the script on all data, you should test the script on a few examples.

You can link a single entity given it's `rdfs:label`:

```
python core/pwb.py link_wikidata_cmtq.py --label "Michel Côté"
```

Afterwards, you can try to link new entities, but limiting the number of edits with `limit`:

```
python core/pwb.py link_wikidata_cmtq.py --limit 10
```

Once satisfied, you can proceed into the next section.

## Execute

Before executing the following command, be sure to test the script with a few examples. Read the last section.

To completely apply the script, execute the bot with `make run` or `python core/pwb.py link_wikidata_cmtq.py`

## Licence

``wikidata_cmtq_linking`` is licenced in [GPLv3](https://opensource.org/licenses/gpl-3.0.html). See [LICENSE](./LICENSE) for a complete copy of the LICENSE.
