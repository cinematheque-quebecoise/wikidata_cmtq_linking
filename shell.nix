let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      python38
      git
    ];

    shellHook = ''
      python -m venv .venv
      source .venv/bin/activate
      pip install -r requirements.txt

      git submodule init
      git submodule update
    '';
  }
